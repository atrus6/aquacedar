//Import required libaries.
#include <ESP8266WiFi.h>

//WiFi parameters
const char* ssid = "Timchi-Net";
const char* password = "timchi loves me";

void setup() {
  //Start Serial
  Serial.begin(115200);
  
  //Connect to WiFi
  WiFi.begin(ssid, password);
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.println("WiFi connected");
  
  //Print the IP address
  Serial.println(WiFi.localIP());
}

void loop() {
  // put your main code here, to run repeatedly:

}
